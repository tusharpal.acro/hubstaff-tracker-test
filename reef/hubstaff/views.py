import json
import requests
import dateutil.parser
from datetime import datetime, timedelta, date

from django.views import View
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin

def unique(array):
    return list(set(array))

def start_time(date_offset):
    return (date.today() - timedelta(days=date_offset)).isoformat()

def stop_time(date_offset):
    return (date.today() - timedelta(days=(date_offset-1))).isoformat()

def activities_params(request, date_offset):
    start_time_isoformat = start_time(date_offset)
    stop_time_isoformat = stop_time(date_offset)

    activities_params = {
        'start_time': start_time_isoformat,
        'stop_time': stop_time_isoformat,
    }

    return activities_params

def convert_matrix_to_payload(data_matrix, project_names, user_names):
    payload = []
    for project_id, project_users in data_matrix.items():
        temp = []
        temp.append(project_names[project_id])
        for user_id, user_name in user_names.items():
            temp.append(data_matrix[project_id][user_id])
        payload.append(temp)
    return payload

class HomePageHandler(LoginRequiredMixin, View):
    def get(self, request):
        date_offset = int(request.GET.get('date_offset', 1))
        headers = {'Auth-Token': settings.AUTH_TOKEN, 'App-Token': settings.APP_TOKEN}
        
        users      = requests.get('https://api.hubstaff.com/v1/users/', headers=headers)
        projects   = requests.get('https://api.hubstaff.com/v1/projects/', headers=headers)
        activities = requests.get('https://api.hubstaff.com/v1/activities/', headers=headers, params=activities_params(request, date_offset))

        users_data = json.loads(users.text)['users']
        projects_data = json.loads(projects.text)['projects']
        activities_data = json.loads(activities.text)['activities']

        projects_worked = unique([activity['project_id'] for activity in activities_data])
        users_worked = unique([activity['user_id'] for activity in activities_data])

        data_matrix = {}
        project_names = {}
        user_names = {}
        for project in projects_data:
            project_id = project['id']
            if project_id in projects_worked:
                project_names[project_id] = project['name']
                data_matrix[project_id] = {}
                for user in users_data:
                    user_id = user['id']
                    if user_id in users_worked:
                        user_names[user_id] = user['name']
                        data_matrix[project_id][user['id']] = 0

        for activity in activities_data:
            data_matrix[activity['project_id']][activity['user_id']] += 1

        context = {
            'payload': convert_matrix_to_payload(data_matrix, project_names, user_names),
            'user_names': user_names,
            'offset_prev': date_offset+1,
            'offset_next': date_offset-1
        }

        with open('data_json/%s-%s' % (start_time(date_offset), stop_time(date_offset)), 'w') as f:
            f.write(json.dumps(context))

        return render(request, 'data_table.html', context)